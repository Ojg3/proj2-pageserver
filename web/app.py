from flask import Flask, abort, render_template, request
app = Flask(__name__)

@app.route("/<name>")

def hello(name):
	try:
	    return render_template(name),200	#this will take the user to the valid file if entered correctly
	except:
	    if ".." in name or "~" in name:	#this will check if there are special characters in the url
	        abort(403)
	    else:
	        abort(404)			#this will have the 404 error if there isnt a special character 
						#and it is not the correct url
@app.errorhandler(403)
def invalid_input(error):
    return render_template('403.html')

@app.errorhandler(404)
def not_found(error):
    return render_template('404.html')

if __name__ == "__main__":
    app.run(debug=True,host = '0.0.0.0')
