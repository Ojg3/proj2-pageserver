#Author: Otis Greer
#	ogreer@uoregon.edu

#This Docker will use flask to create a web app.py which will take a user to the screen which reads "Hello, this is
#valid" if "name.html" is typed into the url.

#If "name.html" is not typed, then a 404 error will be triggered

#If there are special characters ".." or "~" in the url, then a 403 error will be triggered

#In order to build the docker, you must type docker build -t uocis-flask-demo .

#In order to run the docker, you must type python app.py or docker run -d -p 5000:5000 uocis-flask-demo inside the 
#/web directory
